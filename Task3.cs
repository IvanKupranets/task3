using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam_Task3
{
    class Program
    {
        class Vector
        {
            private int[] vector;
            public Vector(int size)
            {
                vector = new int[size];
                SetVectror();
            }
            public void SetVectror()
            {
                Random rand = new Random(DateTime.Now.Ticks.GetHashCode());
                for (int i = 0; i < vector.Length; i++)
                {
                    vector[i] = rand.Next(0, 3);
                }
            }
            public static bool IsOrtogonal(Vector _vec1, Vector _vec2)
            {
                int scalar = 0;
                for (int i = 0; i < _vec1.vector.Length; i++)
                {
                    scalar += _vec1.vector[i] * _vec2.vector[i];
                }
                if (scalar == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public void DisplayVector()
            {
                Console.Write("vector ");
                foreach (var i in vector)
                {
                    Console.Write(i+ " ");
                }
            }
            public int CountComponentTwo()
            {
                var query = (from i in this.vector where i == 2 select i).Count();
                return query;
            }
            public static Vector SectionOfVectors(Vector _vec1, Vector _vec2)
            {
                if (IsOrtogonal(_vec1, _vec2) != true)
                {
                    Vector _result = new Vector(_vec1.vector.Length);
                    for (int i = 0; i < _vec1.vector.Length; i++)
                    {
                        if ((_vec1.vector[i] == 1 && _vec2.vector[i] == 1) ||
                            (_vec1.vector[i] == 1 && _vec2.vector[i] == 2) ||
                            (_vec1.vector[i] == 2 && _vec2.vector[i] == 1))
                        {
                            _result.vector[i] = 1;
                        }
                        if ((_vec1.vector[i] == 0 && _vec2.vector[i] == 0) ||
                           (_vec1.vector[i] == 0 && _vec2.vector[i] == 2) ||
                           (_vec1.vector[i] == 2 && _vec2.vector[i] == 0))
                        {
                            _result.vector[i] = 0;
                        }
                        if (_vec1.vector[i] == 2 && _vec2.vector[i] == 2)
                        {
                            _result.vector[i] = 2;
                        }
                    }
                    return _result;
                }
                else
                {
                    throw new Exception("These vectors are not ortogonal");
                }
            }
        }
        static void Main(string[] args)
        {
            try
            {
                Vector v1 = new Vector(3);
                Vector v2 = new Vector(3);
                v1.DisplayVector();
                Console.WriteLine();
                v2.DisplayVector();
                Console.WriteLine();
                Console.WriteLine("Are these vectors ortogonal; * {0} *:", Vector.IsOrtogonal(v1, v2));
                Console.WriteLine("Count quantity of elemts '2':* {0} *;", v1.CountComponentTwo());
                Console.WriteLine("Section of vectors v1 and v2");
                Vector v3 = Vector.SectionOfVectors(v1,v2);
                v3.DisplayVector();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
